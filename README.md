### TL;DR

- for now supports only openrc and systemd init systems
- allows for dns traffic to chosen server
- allows for http/https traffic
- limits ssh connection "tries" to 1 per second
- allows for established or related connections
- blocks brute force port scanners
- all invalid connections are being logged


### Instalation

- git clone https://gitlab.com/rmnsa/iptables.git
- cd iptables
- !! BE SURE TO READ THE SCRIPT, IF YOU DONT UNDERSTAND IT ASK SOMEONE TO CHECK IT !!
- sudo sh firewall
